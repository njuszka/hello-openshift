#!/bin/bash

. /opt/hello/.venv/bin/activate

echo 'Sleeping for 1 minute...'
sleep 1m

echo 'Starting hello-openshift...'
cd /opt/hello
uwsgi --ini /etc/hello.ini
